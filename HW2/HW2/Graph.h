#ifndef _GRAPH_H
#define _GRAPH_H
#include <vector>
#include <cmath>
using namespace std;
//const float alpha1 = sqrt(15.0/16.0);
#define alpha1 sqrt(15.0/16.0)

typedef struct _edge{
        int u;
        int v;
        _edge(){}
        _edge(int _u, int _v):u(_u), v(_v){}
	_edge(const struct _edge *e):u(e->u), v(e->v){}
}edge;


class Graph {
public:
	Graph(int numvertices, int numedges);
	~Graph();
	void addEdge(int u, int v, int i);
	edge* getedge();
	int numedges();
	int numvertices();

private:
	edge* edges;
	int iedge;
	int nedges;
	int nvertices;
};
void  parRandCC(int n, int ne, edge *E, int *L, int *M);
void  parRandCC2(int n, int ne, int nl, int *V, edge *E, int *L);
void parRandCC3(int n, int ne, int nl, int *V, edge *E, int *L, bool *phd, int *N, bool *U, int d, int d_max);

#endif // _GRAPH_H

