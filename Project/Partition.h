#ifndef _PARTITION_H
#define _PARTITION_H
#include <vector>
#include <map>

using namespace std;

class Partition {
public:
	Partition getPartition();
	Partition();
	~Partition();
	void printPartition();
	void addVertex(int v, vector<int>  &neighbors);
	int size();
	int getCrossEdges();
	map<int, vector <int> >& getVtoAdjList();
	bool find (int v){
		return (vtoadjList.find(v) != vtoadjList.end());
	} 
private:
//        vector <vector<int> > adjList;
        map<int, vector<int> > vtoadjList;
		int curSize;
};

typedef struct heuristicData {
   int K;
   int C;
   //htype heuristic;
} hData;

typedef struct partitionData{
    int size;
    int Id;
    partitionData() : size(0){}
    partitionData(int id, int s) : Id(id), size(s){}
} pData;

class PartitionComparator
{
public:
  bool operator() (pData &lhs, pData &rhs) 
  {
    return (lhs.size > rhs.size);
  }
};




#endif
