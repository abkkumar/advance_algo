#ifndef _GRAPH_H
#define _GRAPH_H
#include <vector>
#include<stdlib.h>
#include <fstream>
using namespace std;

class Graph {
public:
	Graph(int numvertices, int numedges, int numsources);
	~Graph();
	void addEdge(int u, int v);
	void addSource(int s);
	vector<int>& getNeighbors(int u);
	vector<int>& getSources();
	int numedges();
	int numvertices();
	int numsources();
	void dump_graph(string outfile, int is_sorted);
private:
	vector<int> sources;
	vector<vector<int> > edges;
	int nvertices;
	int nedges;
	int nsources;
};

#endif // _GRAPH_H

