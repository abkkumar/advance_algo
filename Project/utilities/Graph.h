#ifndef _GRAPH_H
#define _GRAPH_H
#include <vector>
#include <string>

using namespace std;

class Graph {
public:
	Graph(int numvertices, int numedges, int numsources);
	~Graph();
	void addEdge(int u, int v);
	void addSource(int s);
	vector<int>& getNeighbors(int u);
	vector<int>& getSources();
	vector<int> order;
	int numedges();
	int numvertices();
	int numsources();
	void get_bfs_order(string outfilename);

private:
	vector<int> sources;
	vector<vector<int> > edges;
	int nvertices;
	int nedges;
	int nsources;
};

#endif // _GRAPH_H

