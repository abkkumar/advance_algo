#include "Graph.h"
#include <iostream>
#include <algorithm>
using namespace std;

Graph::Graph(int numvertices, int numedges, int numsources)
: nvertices(numvertices), nedges(numedges), nsources(numsources) {
	edges.resize(nvertices + 1);
}
Graph::~Graph(){}


void Graph::addEdge(int u, int v){
	vector<int> &adjList = edges[u];
	if(!(std::find(adjList.begin(), adjList.end(), v) != adjList.end())){
		adjList.push_back(v);
	}
}

void Graph::addSource(int s){
	sources.push_back(s);
}

vector<int>& Graph::getNeighbors(int u){
	return edges[u];
}

vector<int>& Graph::getSources(){
	return sources;
}

int Graph::numedges(){
	return nedges;
}

int Graph::numvertices(){
	return nvertices;
}

int Graph::numsources(){
	return nsources;
}

void Graph::dump_graph(string outfile, int is_sorted) {
	fstream file;
	file.open (outfile.c_str(), ios::in | ios::out | ios::trunc);

	if(!file.is_open()) {
		cout<<"Error in opening file"<<endl;
		exit(1);
	}
	int new_nedge = 0;
	
	file<<endl<<endl<<endl<<endl<<endl<<endl<<endl;
	for(int i=1; i<=nvertices; i++) {
		vector<int> &neighbours = edges[i];
		cout<<i<<" :";
		for(int j=0; j<neighbours.size(); j++) {
			if(i==neighbours[j]) 
				continue;
			cout<<neighbours[j]<<"\t";
			if(is_sorted) {
				file << i << " " << neighbours[j]<<endl;
				new_nedge++;
			}else {
				file << i << " " << neighbours[j]<<endl;
				file << neighbours[j] << " " << i <<endl;
				new_nedge++;
				new_nedge++;
			}
		}
	cout<<endl;
	}
	cout<<"New edges cnt:"<<new_nedge<<endl;
	file.seekg(0, ios::beg);
	file << nvertices << " " << new_nedge <<" "<< nsources<<endl;
	file.close();	
}
