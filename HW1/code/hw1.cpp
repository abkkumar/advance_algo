#include "filters.h"
#include "GraphReader.h"
#include "Graph.h"
#include <iostream>
#include <ctime>
#include <cilkview.h>

using namespace std;


int cilk_main(int argc, char *argv[]){
//int main(int argc, char *argv[]){
	//if(argc < 3){
	//	cout << "usage: bfs_parallel <graph input filename> <graph output filename" << endl;
	//	return 1;
	//}

	try {
		//GraphReader *gr = GraphReader::getGraphReader();
		GraphReader *gr = new GraphReader();
		Graph *G = gr->read(argv[1]);
		int cores = cilk::current_worker_count();
		int maxstealattempts = 12, minstealsize = 30;
		bfs_parallel bfs(cores, maxstealattempts, minstealsize);

		string outfilename(argv[2]);

		cilk::cilkview cv;
		//cv.reset();
		int start_time = cilk::get_milliseconds();
		cv.start();
		bfs.execute(G, outfilename);

		cv.stop();
		cv.dump("wikipedia.plt");
		cout << "time of execution is " << (((float)(cilk::get_milliseconds() - start_time))/1000) << "secs" << endl;
		delete G;
		delete gr;	
	}	
	catch (...){
		cout << "invalid file name or graph input format" << endl;
	}
	return 0;
}
