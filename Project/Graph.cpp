#include "Graph.h"
#include <iostream>
#include <queue>
#include <stdlib.h>
#include "Partition.h"
#include <algorithm>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <math.h>

using namespace std;

Graph::Graph(int numvertices, int numedges, int numsources)
: nvertices(numvertices), nedges(0), nsources(numsources) {
	edges.resize(nvertices + 1);
}
Graph::~Graph(){}


void Graph::addEdge(int u, int v){
        vector<int> &adjList = edges[u];
        if((u != v) && !(std::find(adjList.begin(), adjList.end(), v) != adjList.end())){
		nedges++;
                adjList.push_back(v);
        }
}



void Graph::addSource(int s){
	sources.push_back(s);
}

vector<int>& Graph::getNeighbors(int u){
	return edges[u];
}

vector<int>& Graph::getSources(){
	return sources;
}

int Graph::numedges(){
	return nedges;
}

int Graph::numvertices(){
	return nvertices;
}

int Graph::numsources(){
	return nsources;
}


void Graph::dumpUnsort(string &outfile) {
	dumpGraph(outfile, 0);
}

void Graph::dumpSort(string &outfile) {
	dumpGraph(outfile, 1);
}

void Graph::dumpGraph(string &outfile, int is_sorted) {
        fstream file;
        file.open (outfile.c_str(), ios::in | ios::out | ios::trunc);

        if(!file.is_open()) {
                cout<<"Error in opening file"<<endl;
                exit(1);
        }

        file << nvertices << " " << nedges*2 <<" "<< nsources<<endl;
        for(int i=1; i<=nvertices; i++) {
                vector<int> &neighbours = edges[i];
                for(int j=0; j<neighbours.size(); j++) {
                        if(is_sorted) {
                                file << i << " " << neighbours[j]<<endl;
                        } else {
                                file << i << " " << neighbours[j]<<endl;
                                file << neighbours[j] << " " << i<<endl;
                        }
                }
        }
	file.close();
}


int Graph::numCrossEdges(){
	int ncedges = 0;
	for(int i = 0; i < partitions.size(); i++){
		//cout<<"Partition :"<<i<<endl;
		ncedges += partitions[i].getCrossEdges();
	}
	return ncedges;
}

void 
Graph::printAllPartition() {
#if 0
        for(int i=0; i<partitions.size(); i++) {
                cout<<"partition: "<< i<<"\t";
                partitions[i].printPartition();
				//cout<<"Partition"<<i<<" Size:"<<partitions[i].size();
                cout<<endl;
        }
#endif
	cout << "********** PRINTING CROSS EDGES *************" << endl << endl;
	int ncedges = numCrossEdges();
	cout << "vertics = " << nvertices <<"," << "edges = " << nedges <<endl;
	cout << "ncedges = " << ncedges/2 << endl;
	float perc = (ncedges*1.0)/nedges;
	printf("perc %.2f \n", perc);
}

void 
Graph::dumpVertexAndPartition() {
		cout << nvertices << " " << partitions.size() << endl;
        for(int i=0; i<partitions.size(); i++) {
				map<int, vector<int> > vtoadjList = partitions[i].getVtoAdjList();
				for( map<int, vector <int> >::iterator ii=vtoadjList.begin(); ii!=vtoadjList.end(); ++ii) {
                	int v_id= (*ii).first;
					cout << v_id << " " << i <<endl;
            	}
        }
}

void
Graph::writeAsMetis(string &filename){
	// assuming graph is undirected
	fstream file;
        file.open (filename.c_str(), fstream::out);
	cout<<"Edges in Metis :"<<nedges<<endl;
	if(file.is_open()) {
		file << nvertices << " " << nedges/2 << endl;
		for(int i = 1; i <= nvertices; i++){
			vector<int> &neighbors = getNeighbors(i);
			for(int j = 0; j < neighbors.size(); j++){
				file << neighbors[j] << " ";
			}		
			file << endl;
		} 
		file.close();
	} else {
		cout<<"Error while opening file in WriteMetis"<<endl;
	}
}

bool 
Graph::partition(int K, int C, htype type, WeightType wt) {
		//cerr << "type : " << type <<endl ;
        switch(type) {
                //case Hashing:
                case Hashing:
                        hashPartition(K);
                        break;
                //case BalanceBig:
                case Balanced:
			balancedPartition(K);
                        break;
		case DeterministicGreedy:
			deterministicGreedy(K, wt);
			break;
                Default:
                        cout<<"Default Error"<<endl;
                        break;
        }
        return true;
}

bool
Graph::hashPartition(int K) {
	partitions.resize(K);
	for(int i = 1; i<= nvertices; i++) {
		int u = order[i];
		int partId = u%K;
		std::vector<int> &neighbors = getNeighbors(u);
		partitions[partId].addVertex(u, neighbors);
	}
	return true;
}


bool
Graph::balancedPartition(int K){
	vector<pData> pd;
	pd.resize(K);
	partitions.resize(K);
	for(int i=0; i<pd.size(); i++){
		pd[i].Id = i;
	}
	typedef priority_queue<pData, vector<pData>, PartitionComparator > prioque;
	prioque q(pd.begin(), pd.end());
	//priority_queue<Partition> q (PartitionComparator, &partitions);
	//cout<<"BalPar num ver:"<<nvertices<<endl;
	for(int i = 1; i<= nvertices; i++) {
		int u = order[i];
		std::vector<int> &neighbors = getNeighbors(u);
		pData minP = q.top();	
		//cout << "details : " << minP.size <<" " <<  minP.Id <<endl;
		partitions[minP.Id].addVertex(u, neighbors);
		pData newData(minP.Id, minP.size + 1);
		q.pop();
		q.push(newData);
	}
	return true;
}

float 
Graph::getUnweightedGreedyWeight(int pid, int C){
	return 1.0F;
}

float
Graph::getLinearGreedyWeight(int pid, int C){
	return (1 - (partitions[pid].size()*1.0/C));
}

float
Graph::getExpoGreedyWeight(int pid, int C){
        return (1 - exp (partitions[pid].size()*1.0 - C));
}


#define abs(val) ((val) > 0 ? val : -(val))
  
bool
Graph::deterministicGreedy(int K, WeightType wt) {
	//cout << "calling determinstic greedy" << endl;
	int C = ceil((nvertices*1.05) / K) ;
	int max = (nvertices*1.0/(C-1))*1.2;
	partitions.resize(K);
	srand(time(NULL));
	for (int v = 1; v <= nvertices; v++) {
	//Allows bfs or random ordering
		int u = order[v];
		
		float w[K];
		int count[K];
		memset(w, 0.0, sizeof(w));
		memset(count, 0, sizeof(count));
		switch(wt){
			case UnweightedGreedy:
				for(int p = 0; p < K; p++){
					w[p] = getUnweightedGreedyWeight(p, C);
				}
				break;
			case LinearWeighted:
				for(int p = 0; p < K; p++){
					w[p] = getLinearGreedyWeight(p, C);
				}
				break;
			case ExpWeighted:
				for(int p = 0; p < K; p++){
					w[p] = getExpoGreedyWeight(p, C);
				}
				break;
			default:
				cout << "not handled as if now " << wt << endl;
		}

		vector<int> & neighbors = getNeighbors(u);
		//cout<<"Vertex :"<<u<<" neighbors_size :"<<neighbors.size()<<endl;
		for(int j = 0 ; j < neighbors.size(); j++) {
			int v = neighbors[j];
			for(int p = 0 ; p < K; p++){
				if(partitions[p].find(v)){
					count[p]++;
					//cout <<"Pid :"<<p<<" count++ = " << count[p] << endl;
					break;		
				}
			}
		}

		// not keeping priority queue for the momenet
		// int maxpartid = 0;
		vector<int> pvec;
		pvec.reserve(K);
		for(int p = 0; p < K; p++){
			if(w[p] > 0) {
				if(pvec.size() > 0){
					float diff = count[p]*w[p] - count[pvec[0]]*w[pvec[0]];
					//cout << "diff is " << diff << endl;
					if(abs(diff) < 0.001){
						pvec.push_back(p);
					} else if (diff > 0) {
						pvec.clear();
						pvec.push_back(p);
					}
				} else {
					pvec.push_back(p);
				}
			}
		}	
		int maxpartid = -1;
			int rval = rand();
			int r = rval % pvec.size();
			maxpartid = pvec[r];

		//cout << "partition assigned for " << u << " is " << maxpartid 
		//	<< " and vec size is " << pvec.size() << endl;
	
		partitions[maxpartid].addVertex(u, neighbors);
	}

	return true;
}


void Graph::get_bfs_order(string outfilename){
        fstream outfile(outfilename.c_str(), ios_base::out);
    order.clear();
    std::vector<int> d(nvertices + 1);
        for (int i = 0; i < nvertices + 1; i++){
                d[i] = nvertices;
        }


        for(int v = 1; v < d.size(); v++){
        if(d[v] != nvertices) {
            continue;
        }

        d[v] = 0;
        queue<int> q;
        q.push(v);
        order.push_back(v);
        outfile << v << endl;

        while (!q.empty()){
            int u = q.front();
            q.pop();

            std::vector<int> &neighbors = edges[u];
            for(int i = 0; i < neighbors.size(); i++){
                if(d[neighbors[i]] == nvertices){
					d[neighbors[i]] = d[u] + 1;
                    q.push(neighbors[i]);
                    order.push_back(neighbors[i]);
                    outfile << neighbors[i] << endl;
                }
            }
        }
    }

}

void Graph::get_dfs_order(string outfilename){
        fstream outfile(outfilename.c_str(), ios_base::out);

        std::vector<int> d(nvertices + 1);
        for (int i = 0; i < nvertices + 1; i++){
                d[i] = nvertices;
        }


        for(int v = 1; v < d.size(); v++){
                if(d[v] != nvertices) {
                        continue;
                }

                d[v] = 0;
                stack<int> s;
                s.push(v);

				while (!s.empty()){
						int u = s.top();
						s.pop();
						order.push_back(u);
						outfile << u << endl;
						std::vector<int> &neighbors = edges[u];
						for(int i = 0; i < neighbors.size(); i++){
								if(d[neighbors[i]] == nvertices){
										d[neighbors[i]] = d[u] + 1;
										s.push(neighbors[i]);
								}
						}
				}
    }
}



