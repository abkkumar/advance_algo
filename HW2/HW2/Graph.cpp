#include "Graph.h"
#include <iostream>

using namespace std;

Graph::Graph(int numvertices, int numedges)
: nvertices(numvertices), nedges(numedges) {
	edges = new edge[nedges];
}
Graph::~Graph(){
	delete [] edges;
}

void Graph::addEdge(int u, int v, int index){
	edges[index] = edge(u, v);
}

edge* Graph::getedge(){
        return edges;
}

int Graph::numedges(){
	return nedges;
}

int Graph::numvertices(){
	return nvertices;
}

