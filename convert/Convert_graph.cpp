#include<iostream>
#include<string.h>
#include<stdlib.h>
#include "GraphReader.h"
#include "Graph.h"
using namespace std;
#define MAX_FILE_LEN 50

class ConvertGraph {
public:
private:
};


//Input: exe <input-file> <output-file> <is_sorted>
int main(int argc, char *argv[]) {
	if(argc<=3) {
		cout<<"Usage: exe <input-file> <output-file> <is_sorted(1 or 0)>"<<endl;
		exit(1);
	} else {
		GraphReader *gr = new GraphReader();
		string infile(argv[1]);
		string outfile(argv[2]);
		Graph *Graph = gr->read(infile, outfile, 0);
		Graph->dump_graph(argv[2], 0);
		if(atoi(argv[3])==1) {
			Graph = gr->read(infile, outfile, 1);
			Graph->dump_graph(argv[2], 1);
		}
	}	
	
	return 0;
}
