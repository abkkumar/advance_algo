# Makefile to build 
# --- macros
CC=g++ -g
OBJECTS1= Graph.o  GraphReader.o Partition.o grapPartitionDemo.o


# --- targets
all:    grapPartitionDemo 
grapPartitionDemo:   $(OBJECTS1) 
	$(CC) -O3 -g -o grapPartitionDemo $(OBJECTS1) 

grapPartitionDemo.o:  grapPartitionDemo.cpp
	$(CC) -O3 -g -c grapPartitionDemo.cpp 

Graph.o: Graph.cpp
	$(CC) -O3 -g -c Graph.cpp 

GraphReader.o: GraphReader.cpp
	$(CC) -O3 -g -c GraphReader.cpp 

Partition.o: Partition.cpp
	$(CC) -O3 -g -c Partition.cpp


# --- remove binary and executable files
clean:
	rm -f grapPartitionDemo $(OBJECTS1) $(OBJECTS2)
