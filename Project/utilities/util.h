#ifndef  _UTIL_H
#define  _UTIL_H

#include <vector>
#include <iostream>

using namespace std;

template<class T>
class QueueEx {
public :
	QueueEx()
	: q(new vector<T> ), ownq(q), s(0), e(0) {}

	/*QueueEx(vector<T> *argq, int start, int end) 
	: q(argq), s(start), e(end), allocated(false){}


	QueueEx(QueueEx<T> &qin): q(qin.q), s(qin.s), e(qin.e), allocated(false){
		
	}*/
	~QueueEx(){
		/*if(allocated == true){
			delete q;
		}
		while(!garbage.empty()){
			delete garbage.front();
			garbage.pop_front();
		}*/
		delete ownq;
	}

	T pop_front() {
		s++;
		return (*q)[s-1];
	}
	
	T front(){
		return (*q)[s];
	}
	
	void push_back(T elem) {
		q->push_back(elem);
		e++;	
	}

	bool empty(){
		return (s >= e);
	}

	int size(){
		return (e - s);
	}

	void clear(){
		q->clear();
		s = 0;
		e = 0;
	}

	void steal(QueueEx<T> &qin){
		int mid = (qin.e - qin.s)/2 + 1;
	        q = qin.q;
                s = mid;
                e = qin.e;
                qin.e = mid;
	}
	
	vector<T> *q;
	vector<T> *ownq;
	int s;
	int e;
	bool allocated; 
};

#endif // _UTIL_H
	
