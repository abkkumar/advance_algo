		#! /bin/bash 
		#-V	 			#Inherit the submission environment
		#$ -cwd	 			# Start job in submission directory
		#$ -N a		 		# Job Name
		#$ -j y	 			# Combine stderr and stdout (If you are using this option, ignore the next line)
		#$ -e error_e		# Name of the error file in case your code is wrong
		#$ -o output_a		# Name of the output file
		#$ -pe 12way 24	 	# Requests 12 tasks/node, 24 cores total
		#$ -q normal	 	# Queue name normal
		#$ -l h_rt=01:30:00	# Run time (hh:mm:ss) - 1.5 hours
		#$ -M	 			# Address for email notification
		#$ -m be	 		# Email at Begin and End of jobexport PATH=$PATH:$HOME/cilk/bin
		#./hw1 'samples/sample-01-in.txt' 'sample-01.txt' > bfs_parallel_output 2>&1
		./hw1 'turn-in/wikipedia-in.txt' 'wikipedia' > bfs_parallel_wiki 2>&1
		#./hw1 'turn-in/kkt_power-in.txt' 'kkt_power-out.txt' > bfs_parallel_kkt_power 2>&1
		#./bfs_parallel 'sample.txt' 'sample.out' > output 2>&1
