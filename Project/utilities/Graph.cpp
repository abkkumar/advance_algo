#include "Graph.h"
#include <queue>
#include <iostream>
#include <fstream>

using namespace std;

Graph::Graph(int numvertices, int numedges, int numsources)
: nvertices(numvertices), nedges(numedges), nsources(numsources) {
	edges.resize(nvertices + 1);
	for(int i=0; i<=nvertices; i++){
		order.push_back(i);
	}
}
Graph::~Graph(){}

void Graph::addEdge(int u, int v){
	edges[u].push_back(v);
}

void Graph::addSource(int s){
	sources.push_back(s);
}

vector<int>& Graph::getNeighbors(int u){
	return edges[u];
}

vector<int>& Graph::getSources(){
	return sources;
}

int Graph::numedges(){
	return nedges;
}

int Graph::numvertices(){
	return nvertices;
}

int Graph::numsources(){
	return nsources;
}

void Graph::get_bfs_order(string outfilename){
        fstream outfile(outfilename.c_str(), ios_base::out);
	order.clear();
    std::vector<int> d(nvertices + 1);
        for (int i = 0; i < nvertices + 1; i++){
                d[i] = nvertices;
        }


        for(int v = 1; v < d.size(); v++){
        if(d[v] != nvertices) {
            continue;
        }

        d[v] = 0;
        queue<int> q;
        q.push(v);
		order.push_back(v);
        outfile << v << endl;

        while (!q.empty()){
            int u = q.front();
            q.pop();

            std::vector<int> &neighbors = edges[u];
			for(int i = 0; i < neighbors.size(); i++){
                if(d[neighbors[i]] == nvertices){
                    d[neighbors[i]] = d[u] + 1;
                    q.push(neighbors[i]);
                    order.push_back(neighbors[i]);
                    outfile << neighbors[i] << endl;
                }
            }
        }
    }

}


