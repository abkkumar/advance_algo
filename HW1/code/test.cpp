#include <iostream>
#include "util.h"

using namespace std;

int main(){

	QueueEx<int> q1;
	int i  = 100;
	while( i > 0){
		q1.push_back(i);
		i--;
	}


	vector<int> v1;
	v1.push_back(1);
	cout << &v1[0] << endl;
	
	vector<int> v2;
        v2.push_back(1);
        cout << &v2[0] << endl;

	vector<int> &v3 = v1;
	cout << &v3[0] << endl;
	v3 = v2;
	cout << &v3[0] << endl;

	QueueEx<int> q2;
	q2.steal(q1);

	int q1sz = q1.size();
	int q2sz = q2.size();	
	cout << "q1 size, q1 q size, q2 size, q2 q size is (" <<
		q1sz << ", " << q1.q->size() << ", " << q2sz
		<< ", " <<  q2.q->size() << ")" << &(*q1.q)[0] << " and " << &(*q2.q)[0] << endl;  
	return 0;
}
