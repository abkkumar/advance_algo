#ifndef _GRAPH_READER_H
#define _GRAPH_READER_H

#include <string>
#include <vector>
#include "Graph.h"

class GraphReader{

	public:
		GraphReader(){}
		~GraphReader(){}
		//static GraphReader* getGraphReader();

		//Graph* read(string filename);
		Graph* read(string &filename, string &outfile, int check);

	private:
		

	private:
		//static GraphReader* gr;
};

#endif // _GRAPH_READER_H

