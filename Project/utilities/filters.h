#ifndef _FILTER_H
#define _FILTER_H

#include "Graph.h"
#include <fstream>
#include <list>
#include "util.h"

class filter {
public:
	filter(){}
	~filter(){}

protected:

	unsigned long long 
	computeChecksum(vector<int> &d);
};	




class bfs_serial : public filter{
public:
	bfs_serial();
	~bfs_serial();

	void execute(Graph *g, string outfile);
	void dump_bfs_order(Graph *g, string outfilename);

private:
	void _bfs_source(Graph *g, int s, fstream &outfile);
	
};


class dfs_serial : public filter{
public:
        dfs_serial();
        ~dfs_serial();

        //void execute(Graph *g, string outfile);
        void dump_dfs_order(Graph *g, string outfilename);

//private:
        //void _bfs_source(Graph *g, int s, fstream &outfile);

};

#endif // _FILTER_H
