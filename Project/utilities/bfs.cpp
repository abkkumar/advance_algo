#include "filters.h"
#include "GraphReader.h"
#include "Graph.h"
#include <iostream>

using namespace std;


int main(int argc, char *argv[]){
	if(argc < 3){
		cout << "usage: bfs_serial <graph input filename> <graph output filename" << endl;
		return 1;
	}

	try {
		//GraphReader *gr = GraphReader::getGraphReader();
		GraphReader *gr = new GraphReader();
		Graph *G = gr->read(argv[1]);
		bfs_serial bfs;
		string outfilename(argv[2]);

		//bfs.execute(G, outfilename);
		//bfs.dump_bfs_order(G, outfilename);
		G->get_bfs_order(outfilename);

		delete G;
		delete gr;	
	}	
	catch (...){
		cout << "invalid file name or graph input format" << endl;
	}
	return 0;
}
