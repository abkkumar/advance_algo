1. MPI_COMM_WORLD : check why this is used.

2.Arguments for MPI Routine:
(buffer, data count, data type, destination)

• Buffer: the name of a variable (including arrays and structures) that is to be sent/received. For C programs, this argument is passed by reference and usually must be prepended with an ampersand: &var1
• Data Count: number of data elements to be sent or received
• Data Type: could be elementary data type or derived
• Destination: the process where a message is to be delivered


Arguments for MPI Routine:
(source, tag, status, request)

• Source: indicates the process from which the message originated
• Tag: non-negative integer value to uniquely identify a message
• Status: for a receive operation, indicates the source and tag of a message
• Request: a unique “request number” issued by the system that can be used to check if a particular category of operation has completed or not (more on this later)

3. MPI_Wtime is a timer routine that returns elapsed wall clock time in seconds.
MPI_Wtime()


4. MPI_Wait is a blocking routine
MPI_Wait (&request, &status)

4. Modes of Communication
• Point-to-Point : 
	1.One process performs a send operation and the other task performs a matching receive operation.
	2. There should be a matching receive routine for every send routine
		– If a send is not paired with a matching receive then the code will have a deadlock
– Blocking
	– A blocking receive only "returns" after the data has arrived and is ready for use by the program
	– A blocking send routine returns after it is safe to modify the application buffer for reuse
	– A blocking send can be either synchronous or asynchronous

– Non-Blocking
	– Non-blocking send and receive routines will return almost immediately
	– It is unsafe to modify the application buffer until you know for a fact that the requested non-blocking
		 operation was actually performed 

– Synchronous
– Buffered
– Combined
• Collective
