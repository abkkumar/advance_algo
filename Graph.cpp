#include "Graph.h"
#include <iostream>
#include <queue>
#include <stdlib.h>
#include "Partition.h"
#include <algorithm>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <deque>
#include <assert.h>

using namespace std;

Graph::Graph(int numvertices, int numedges, int numsources)
: nvertices(numvertices), nedges(0), nsources(numsources), delayassign(false) {
	edges.resize(nvertices + 1);
}
Graph::~Graph(){}


void Graph::addEdge(int u, int v){
        vector<int> &adjList = edges[u];
        if((u != v) && !(std::find(adjList.begin(), adjList.end(), v) != adjList.end())){
		nedges++;
                adjList.push_back(v);
        }
}



void Graph::addSource(int s){
	sources.push_back(s);
}

vector<int>& Graph::getNeighbors(int u){
	return edges[u];
}

vector<int>& Graph::getSources(){
	return sources;
}

int Graph::numedges(){
	return nedges;
}

int Graph::numvertices(){
	return nvertices;
}

int Graph::numsources(){
	return nsources;
}


void Graph::dumpUnsort(string &outfile) {
	dumpGraph(outfile, 0);
}

void Graph::dumpSort(string &outfile) {
	dumpGraph(outfile, 1);
}

void Graph::dumpGraph(string &outfile, int is_sorted) {
        fstream file;
        file.open (outfile.c_str(), ios::in | ios::out | ios::trunc);

        if(!file.is_open()) {
                cout<<"Error in opening file"<<endl;
                exit(1);
        }

        file << nvertices << " " << nedges*2 <<" "<< nsources<<endl;
        for(int i=1; i<=nvertices; i++) {
                vector<int> &neighbours = edges[i];
                for(int j=0; j<neighbours.size(); j++) {
                        if(is_sorted) {
                                file << i << " " << neighbours[j]<<endl;
                        } else {
                                file << i << " " << neighbours[j]<<endl;
                                file << neighbours[j] << " " << i<<endl;
                        }
                }
        }
	file.close();
}


int Graph::numCrossEdges(){
	int ncedges = 0;
	for(int i = 0; i < partitions.size(); i++){
		//cout<<"Partition :"<<i<<endl;
		ncedges += partitions[i].getCrossEdges();
	}
	return ncedges;
}

void 
Graph::printAllPartition() {
        for(int i=0; i<partitions.size(); i++) {
                //cout<<"partition: "<< i<<"\t";
                //partitions[i].printPartition();
		cout<<"Partition"<<i<<" Size:"<<partitions[i].size();
                cout<<endl;
        }

	cout << "********** PRINTING CROSS EDGES *************" << endl << endl;
	int ncedges = numCrossEdges();
	cout << "vertics = " << nvertices <<"," << "edges = " << nedges <<endl;
	cout << "ncedges = " << ncedges/2 << endl;
	float perc = (ncedges*1.0)/nedges;
	printf("perc %.4f \n", perc);
}

void
Graph::writeAsMetis(string &filename){
	// assuming graph is undirected
	fstream file;
        file.open (filename.c_str(), fstream::out);
	cout<<"Edges in Metis :"<<nedges<<endl;
	if(file.is_open()) {
		file << nvertices << " " << nedges/2 << endl;
		for(int i = 1; i <= nvertices; i++){
			vector<int> &neighbors = getNeighbors(i);
			for(int j = 0; j < neighbors.size(); j++){
				file << neighbors[j] << " ";
			}		
			file << endl;
		} 
		file.close();
	} else {
		cout<<"Error while opening file in WriteMetis"<<endl;
	}
}

bool 
Graph::partition(int K, int C, htype type, WeightType wt) {
		cout << "type : " << type <<endl ;
        switch(type) {
                //case Hashing:
                case Hashing:
                        hashPartition(K);
                        break;
                //case BalanceBig:
                case Balanced:
			balancedPartition(K);
                        break;
		case DeterministicGreedy:
			deterministicGreedy(K, wt);
			break;
                Default:
                        cout<<"Default Error"<<endl;
                        break;
        }
        return true;
}

bool
Graph::hashPartition(int K) {
	partitions.resize(K);
	for(int i = 1; i<= nvertices; i++) {
		int partId = i%K;
		std::vector<int> &neighbors = getNeighbors(i);
		partitions[partId].addVertex(i, neighbors);
	}
	return true;
}


bool
Graph::balancedPartition(int K){
	vector<pData> pd;
	pd.resize(K);
	partitions.resize(K);
	for(int i=0; i<pd.size(); i++){
		pd[i].Id = i;
	}
	typedef priority_queue<pData, vector<pData>, PartitionComparator > prioque;
	prioque q(pd.begin(), pd.end());
	//priority_queue<Partition> q (PartitionComparator, &partitions);
	//cout<<"BalPar num ver:"<<nvertices<<endl;
	for(int i = 1; i<= nvertices; i++) {
		std::vector<int> &neighbors = getNeighbors(i);
		pData minP = q.top();	
		//cout << "details : " << minP.size <<" " <<  minP.Id <<endl;
		partitions[minP.Id].addVertex(i, neighbors);
		pData newData(minP.Id, minP.size + 1);
		q.pop();
		q.push(newData);
	}
	return true;
}

float 
Graph::getUnweightedGreedyWeight(int pid, int C){
	return 1.0F;
}

float
Graph::getLinearGreedyWeight(int pid, int C){
	return (1 - (partitions[pid].size()*1.0/C));
}

float
Graph::getExpoGreedyWeight(int pid, int C){
        return (1 - exp (partitions[pid].size()*1.0 - C));
}


#define abs(val) ((val) > 0 ? val : -(val))
#define MAX_DELAY 5000


// returns partitionid
int
Graph::_getPartitionDG(int u, int K, int C, WeightType wt, bool delay){
	float w[K];
	int count[K];
	memset(w, 0.0, sizeof(w));
	memset(count, 0, sizeof(count));
	switch(wt){
		case UnweightedGreedy:
			for(int p = 0; p < K; p++){
				w[p] = getUnweightedGreedyWeight(p, C);
			}
			break;
		case LinearWeighted:
			for(int p = 0; p < K; p++){
				w[p] = getLinearGreedyWeight(p, C);
			}
			break;
		case ExpWeighted:
			for(int p = 0; p < K; p++){
				w[p] = getExpoGreedyWeight(p, C);
			}
			break;
		default:
			cout << "not handled as if now " << wt << endl;
	}

	vector<int> & neighbors = getNeighbors(u);
	//cout<<"Vertex :"<<u<<" neighbors_size :"<<neighbors.size()<<endl;
	for(int j = 0 ; j < neighbors.size(); j++) {
		int v = neighbors[j];
		for(int p = 0 ; p < K; p++){
			if(partitions[p].find(v)){
				count[p]++;
				//cout <<"Pid :"<<p<<" count++ = " << count[p] << endl;
				break;		
			}
		}
	}

	// not keeping priority queue for the momenet
	// int maxpartid = 0;
	vector<int> pvec;
	pvec.reserve(K);
	for(int p = 0; p < K; p++){
		if(w[p] > 0) {
			if(pvec.size() > 0){
				float diff = count[p]*w[p] - count[pvec[0]]*w[pvec[0]];
				//cout << "diff is " << diff << endl;
				if(abs(diff) < 0.001){
					pvec.push_back(p);
				} else if (diff > 0) {
					pvec.clear();
					pvec.push_back(p);
				}
			} else {
				if((!delay) || count[p] != 0)
					pvec.push_back(p);
			}
		}
	}	

	if(pvec.size() == 0){
		return -1;
	}

	int rval = rand();
	int r = rval % pvec.size();
	int maxpartid = pvec[r];

	//cout << "partition assigned for " << u << " is " << maxpartid 
	//	<< " and vec size is " << pvec.size() << endl;
	return maxpartid;
}

void
Graph::_assignvertex(int u, int pid){
	vector<int> & neighbors = getNeighbors(u);
	partitions[pid].addVertex(u, neighbors);
}

  
bool
Graph::deterministicGreedy(int K, WeightType wt) {
	cout << "calling determinstic greedy" << endl;
	int C = ceil((nvertices*1.05) / K) ;
	deque<int> delayvertices;
	//delayvertices.reserve(MAX_DELAY);

	partitions.resize(K);
	srand(time(NULL));
	if(nvertices == 0){
			return true;
	}

	int pid = 0;
	pid = _getPartitionDG(1, K, C, wt, false);
	_assignvertex(1, pid);
	int dcnt = 0;
	int tcnt = 1;
	for (int u = 2; u <= nvertices; u++) {
		pid = _getPartitionDG(u, K, C, wt, delayassign);
		assert(pid >= -1 && pid < K);
		if(pid == -1){
			delayvertices.push_back(u);
			dcnt++;
		} else{
			tcnt++;
			_assignvertex(u, pid);
		}

		if(delayvertices.size() >= MAX_DELAY){
			// process at least 50% of it
			int sz = delayvertices.size()/5;
			for(int i = 0; i < sz; i++){
				int u_delay = delayvertices.front();
				delayvertices.pop_front();
#if 0
				pid = _getPartitionDG(u_delay, K, C, wt, false);
				assert(pid != -1);
				tcnt++;
				assert(pid >= -1 && pid < K);
				_assignvertex(u_delay, pid);
				dcnt--;
#else
				pid = _getPartitionDG(u_delay, K, C, wt, false);
				if(pid == -1){
					delayvertices.push_back(u_delay);
					dcnt++;
				} else{
					tcnt++;
					_assignvertex(u_delay, pid);
				}
#endif
			}
		}

	}

	for(int i = 0; i < delayvertices.size(); i++){
		//int u_delay = delayvertices.front();
		//delayvertices.pop_front();
		dcnt--;
		pid = _getPartitionDG(delayvertices[i], K, C, wt, false);
		assert(pid != -1);
		tcnt++;
		assert(pid >= -1 && pid < K);	
		_assignvertex(delayvertices[i], pid);
	}
	cout << "dcnt = " << dcnt << " tcnt = " << tcnt << endl;
	return true;
}
