#include "GraphReader.h"
#include "Graph.h"
#include <stdlib.h>
#include "Partition.h"
#include "Evaluater.h"
#include <iostream>
#include <ctime>
#include <sstream>
#include <vector>

using namespace std;
#define GEN_UNDIR 1
#define GEN_METIS 0

void usage(){
	cout << "usage: grapPartitionDemo <-h|-p|-g> <graph-input-file>  <graph-output-file|heurtic type> <sort-option|K> <d|b order_file>" << endl;
	cout << "-p = to partition, -g = to generate the graph, -h = for help" <<endl;
	cout << "ex: grapPartitionDemo -p sample Balanced|Hashing|DeterministicGreedy:LinearWeighted 5 [orderfile]" << endl;
	cout << "ex: grapPartitionDemo -g sample out 0|1" << endl;
	cout << "ex: grapPartitionDemo -e sample.in  metis.out K " << endl;
	cout << "ex: ./grapPartitionDemo -p data/sample-01-in.txt Balanced 4 d<d:dfs & b:bfs> gdfs1<order file>" << endl;
}
vector<string> &split(const string &s, char delim, vector<std::string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

vector<string> split(const string &s, char delim) {
    vector<std::string> elems;
    return split(s, delim, elems);
}

htype getHtype(string args){
	vector<string> tokens;
    split(args, ':', tokens);
	string htype = tokens[0];
	cerr << htype << endl;
	if(htype.compare("Hashing") == 0){
		return Hashing;
	}
	if(htype.compare("Balanced") == 0){
		return Balanced;
	}
	if(htype.compare("DeterministicGreedy") == 0){
		return DeterministicGreedy;
	}
	else{
		cout << "Unknown type" << endl;
		usage();
		exit(0);
	}

}

WeightType getWeightType(string args){
	vector<string> tokens;  
    split(args, ':', tokens);
	if(tokens.size() > 1) {
			string weightType = tokens[1];
			if(weightType.compare("LinearWeighted") == 0){
					return LinearWeighted;
			}
			else if(weightType.compare("ExpWeighted") == 0){
					return ExpWeighted;
			}
	}
	return UnweightedGreedy;
}

int main(int argc, char *argv[]){
	if(argc < 5){
		usage();
		//cout << "usage: grapPartitionDemo <-p|-g> <graph-input-file>  <graph-output-file|heurtic type> <sort-option|K>" << endl;
		return 1;
	}

	try {
		//int K = atoi(argv[4]), C = atoi(argv[5]);
		
		int K = 12, C = 500;

		string argtype = argv[1];
		GraphReader *gr = new GraphReader();
		string infile(argv[2]);
		Graph *g = NULL;

		if(argtype.compare("-e") == 0){
				string evalfile(argv[3]);
				Evaluater eval;
				g = gr->read(infile);
				eval.cntEdgecuts(evalfile, atoi(argv[4]), g);
		} else 	if(argtype.compare("-g") == 0){
				string outfile(argv[3]);
				if(GEN_UNDIR) {
						g = gr->readDir(infile, outfile);
						g->dumpUnsort(outfile);

						if(atoi(argv[4]) == 1) {
								g = gr->readUndir(infile, outfile);
								g->dumpSort(outfile);
						}
						if(GEN_METIS) {
								g = gr->readUndir(infile, outfile);
								g->writeAsMetis(outfile);
						}
				}
		}
		else if(argtype.compare("-p") == 0){
				string htype(argv[3]);
				K = atoi(argv[4]);
				if(argc > 5){
						string ordering = argv[5];
						string orderFile(argv[6]);
						g = gr->read(infile);
						if(ordering.compare("b") == 0){
								cerr << "calling dfs ordering";
								g->get_bfs_order(orderFile);
						}
						else if(ordering.compare("d") == 0){
								cerr << "calling dfs ordering";
								g->get_dfs_order(orderFile);
						}
				
						//g->partition(K, C, Hashing);
						//g->partition(K, C, Balanced);
						g->partition(K, C, getHtype(htype), getWeightType(htype));
						//g->partition(K, C, DeterministicGreedy, UnweightedGreedy);
						//g->partition(K, C, DeterministicGreedy, LinearWeighted);
						g->printAllPartition();
						//g->dumpVertexAndPartition();
				} else {
						cerr << "Type of order is missing." << endl;
				}
		}
		else{
			usage();
			return 1;
		}
	
		delete g;
		delete gr;	
	}
	catch (...){
		cout << "invalid file name or graph input format" << endl;
	}
	return 0;
}



