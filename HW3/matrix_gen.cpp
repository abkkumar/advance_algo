#include "matrix.h"

void gen_matrix(int n, int max, string fname) {
	ofstream file;
	file.open(fname.c_str(), ios_base::in|ios_base::out|ios_base::trunc);
	if(file.is_open()) {
		file << n << endl;
		for (int i=0; i < (n*n); i++) {
			int num = rand() % 	max+1;
			file << num << endl;
		}
		file.close();
	} else {
		cout << "Error in opening file" << endl;
	}
	return;
}



int main(int argc, char* argv[] ) {
	if(argc >= 5) {
		int in_sz = atoi(argv[1]);

		if(in_sz > 32768) {
			cout<<"Wrong input size. Max: 32768"<<endl;
			exit(1);
		}
		srand(time(NULL));
		gen_matrix(in_sz, atoi(argv[2]), argv[3]);
		gen_matrix(in_sz, atoi(argv[2]), argv[4]);
	} else {
		cout<<"Usage: ./matrix_gen <n: matrix size> <max-input> <output-filename1> <output-file2>" << endl;
	}	

	return 0;
}
