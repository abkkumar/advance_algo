#include "filters.h"
#include "GraphReader.h"
#include "Graph.h"
#include <iostream>

using namespace std;


int main(int argc, char *argv[]){
	if(argc < 3){
		cout << "usage: bfs_serial <graph input filename> <graph output filename" << endl;
		return 1;
	}

	try {
		//GraphReader *gr = GraphReader::getGraphReader();
		GraphReader *gr = new GraphReader();
		Graph *G = gr->read(argv[1]);
		dfs_serial dfs;
		string outfilename(argv[2]);

		//bfs.execute(G, outfilename);
		dfs.dump_dfs_order(G, outfilename);

		delete G;
		delete gr;	
	}	
	catch (...){
		cout << "invalid file name or graph input format" << endl;
	}
	return 0;
}
