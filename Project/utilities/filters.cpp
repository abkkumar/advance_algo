#include "filters.h"
#include <queue>
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <exception>
#include <stack>

using namespace std;



unsigned long long 
filter::computeChecksum(vector<int> &d){
	//cilk::reducer_opadd<unsigned long long  > chksum;
	unsigned long long chksum = 0;

	for(int i = 1; i < d.size(); i++){
		chksum += d[i];
	}

	return chksum;
}	


bfs_serial::bfs_serial(){}
bfs_serial::~bfs_serial(){}

void
bfs_serial::execute(Graph *g, string outfilename){
	if(g == NULL){
		std::cout << "fatal error: g is null" << endl;
		return;
	}
	std::vector<int> &sources = g->getSources();
	fstream outfile(outfilename.c_str(), ios_base::out);
	for(int i = 0; i < sources.size(); i++){
		_bfs_source(g, sources[i], outfile);
	}

}

void
bfs_serial::dump_bfs_order(Graph *g, string outfilename){
        if(g == NULL){
                std::cout << "fatal error: g is null" << endl;
                return;
        }
        fstream outfile(outfilename.c_str(), ios_base::out);
	
	int nvertices = g->numvertices();
	std::vector<int> d(nvertices + 1);
        for (int i = 0; i < nvertices + 1; i++){
                d[i] = nvertices;
        }


        for(int v = 1; v < d.size(); v++){
		if(d[v] != nvertices) {
			continue;
		}

		d[v] = 0;
		queue<int> q;
		q.push(v);
		outfile << v << endl;

		while (!q.empty()){
			int u = q.front();
			q.pop();

			std::vector<int> &neighbors = g->getNeighbors(u);
			for(int i = 0; i < neighbors.size(); i++){
				if(d[neighbors[i]] == nvertices){
					d[neighbors[i]] = d[u] + 1;
					q.push(neighbors[i]);
					outfile << neighbors[i] << endl;
				}
			}
		}
	}

}


void
bfs_serial::_bfs_source(Graph *g, int s, fstream &outfile){
	int nvertices = g->numvertices();
	std::vector<int> d(nvertices + 1);
	int src_bfs_level = 0;

	for (int i = 0; i < nvertices + 1; i++){
		d[i] = nvertices;
	}

	d[s] = 0;
	queue<int> q;
	q.push(s);


	while (!q.empty()){
		int u = q.front();
		q.pop();

		std::vector<int> &neighbors = g->getNeighbors(u);
		for(int i = 0; i < neighbors.size(); i++){
			if(d[neighbors[i]] == nvertices){
				d[neighbors[i]] = d[u] + 1;
				src_bfs_level = d[u] + 1;
				q.push(neighbors[i]);
			}
		}
	}

	unsigned long long checksum = computeChecksum(d);
	//cout << "checksum is " << checksum << " and src_bfs_level is " << src_bfs_level << endl;
	outfile << src_bfs_level << " " << checksum << endl;

}


dfs_serial::dfs_serial(){}
dfs_serial::~dfs_serial(){}

void
dfs_serial::dump_dfs_order(Graph *g, string outfilename){
        if(g == NULL){
                std::cout << "fatal error: g is null" << endl;
                return;
        }

        fstream outfile(outfilename.c_str(), ios_base::out);

        int nvertices = g->numvertices();
        std::vector<int> d(nvertices + 1);
        for (int i = 0; i < nvertices + 1; i++){
                d[i] = nvertices;
        }


        for(int v = 1; v < d.size(); v++){
                if(d[v] != nvertices) {
                        continue;
                }

                d[v] = 0;
                stack<int> s;
                s.push(v);

                while (!s.empty()){
                        int u = s.top();
                        s.pop();
			outfile << u << endl;
		
                        std::vector<int> &neighbors = g->getNeighbors(u);
                        for(int i = 0; i < neighbors.size(); i++){
                                if(d[neighbors[i]] == nvertices){
                                        d[neighbors[i]] = d[u] + 1;
                                        s.push(neighbors[i]);
                                }
                        }
                }
	}
}

