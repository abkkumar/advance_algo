#include <stdio.h>
#include <vector>
#include "mpi.h"
#include <iostream>
#include <map>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <ctime>  
 
using namespace std;
/* This is the process ID, starting from 0 to n-1.*/
//int rank;

typedef struct _pendingmsg {
	int u;
	int pid; // partition id, rank of source 
	_pendingmsg(int _u, int _pid) : u(_u), pid(_pid) {}
} pendingmsg;


int MPI_graph_read(map <int, vector <int> > &adj_list, char *filename) {
	fstream infile;
	do {
		infile.open(filename);

	} while((infile.rdstate() & std::ifstream::failbit ) != 0 );

	int vertices = 0;
	if(infile.is_open()) {

		/*cntv : num vertex ,v is vertex, p is partid*/
		int u, v, edges, sources;

		infile >> vertices >> edges;

		while(edges--) {

			infile >> u >> v;
			if (adj_list.find(u) != adj_list.end()) {
				adj_list[u].push_back(v);
			} 
		}

		infile.close();
	} else {
		cout<<"Issue in opening file."<<endl;
		exit(1);
	}
	return vertices;
}


void MPI_partid_read(map <int, int> &partid, map<int , vector<int> > &adj_list, char *filename, int rank) {
	fstream infile;
	int attempts = 0;
	do {
		infile.open(filename);
		cout << "attempts : " << attempts++ << endl;

	} while((infile.rdstate() & std::ifstream::failbit ) != 0 );


	if(infile.is_open()) {

		/*cntv : num vertex ,v is vertex, p is partid*/
		int v, p, cntv, npart;
		infile >> cntv >> npart;

		while(cntv--) {
			infile >> v >> p;
			//V should not be present in the map.
			cout << v << p << rank << endl;
			if (p==rank) {
				vector <int> adj;
				adj_list[v] = adj;
			} else {
				partid[v] = p;
			}
		}
		//sleep(15);
		infile.close();
	} else {
		cout<<"Issue in opening file."<<endl;
		exit(1);
	}
}

/*Input: ./mpi_pr graph partid*/
int main(int argc, char *argv[])
{

	if(argc < 3) {
		cout<<"input args wrong."<<endl;
		exit(1);
	}

	double stimer, etimer;
	struct tm y2k;
	double seconds;

	/*vertex(if this lie in this part) : adj_list*/
	map <int , vector <int> > adj_list;
	map <int, float>	pr;
	/*Vertex : partid*/
	map <int, int> partid;

	int vertices;

	MPI_Status status;
	int num, rank, size, tag, next, from;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	cout<< " Rank :" << rank <<endl;
	MPI_partid_read(partid, adj_list, argv[2], rank);
	vertices = MPI_graph_read(adj_list, argv[1]);


	string fname;
	stringstream ss;
	ss << rank;
	fname = "out" + ss.str();

	ofstream infile(fname.c_str());

	if(infile.is_open()) {
		cout<<"Rank : "<< rank << "Part List :"  <<endl;
		for(map<int, int>::iterator  i=partid.begin();  i != partid.end(); i++) {
			//cout << i->first << " : " << i->second << endl;
			infile <<  i->first << " : " << i->second << endl;
		}

		cout<<"Rank :" << rank << "Adj_list" <<endl;
		for(map<int, vector<int> >::iterator i=adj_list.begin(); i != adj_list.end(); i++) {
			//cout << i->first << " : " << "\t";
			infile << i->first << " : " << "\t";
			for(int j=0; j != i->second.size(); j++) {
				//cout <<  i->second[j] << "\t";
				infile <<  i->second[j] << "\t";
			}
			infile << endl;
			cout<< endl;
		}
	} else {
		cout<<"Error in opening output file in :" << rank <<endl;
		//assert(printf("Issue in opening output in %d", rank) && 0);
		exit(0);
	}


	MPI_Barrier(MPI_COMM_WORLD);
	//return 0;
	stimer = time(0);
#if 1


	map<int, vector<int> >::iterator i_adjlist = adj_list.begin();
	for( ; i_adjlist != adj_list.end(); i_adjlist++) {
		pr[i_adjlist->first] = 0.0;
	}

	float r = 1.0/vertices;


	vector<pendingmsg> pending_vertices;
	// initialize iterator again to iterate map
	i_adjlist = adj_list.begin();

	cout << " Start" <<endl;
	for( ; i_adjlist != adj_list.end(); i_adjlist++) {
		int u = i_adjlist->first;
		vector<int> &neighbors = i_adjlist->second;
		float nbr_pr = r / neighbors.size();
		for(int j = 0; j < neighbors.size(); j++) {
			int v = neighbors[j];
			if(adj_list.find(v) != adj_list.end()) {
				pr[v] += nbr_pr;
			} else {
				int v_pid = partid[v];
				pendingmsg msg(u, v_pid);
				pending_vertices.push_back(msg);

				// send nbr_pr to v_pid rank with tag v
				printf("Process %d sending %f to %d with tag %d\n", rank, nbr_pr, v_pid, v);
				MPI_Send(&nbr_pr, 1, MPI_FLOAT, v_pid, v, MPI_COMM_WORLD);				
			}
		}

	}

	for(int i = 0; i < pending_vertices.size(); i++){
		float edge_pr = 0.0;
		pendingmsg msg = pending_vertices[i];
		printf("Process %d received %f from %d with tag %d\n", rank, edge_pr, msg.pid, msg.u);
		MPI_Recv(&edge_pr, 1, MPI_FLOAT, msg.pid, msg.u, MPI_COMM_WORLD, &status);
		pr[msg.u] += edge_pr;
	}

    i_adjlist = adj_list.begin();
    for( ; i_adjlist != adj_list.end(); i_adjlist++) {
		cout << i_adjlist->first << " : "<<  pr[i_adjlist->first] << endl;
    }

	MPI_Barrier(MPI_COMM_WORLD);	
	etimer = time(0);
	seconds = etimer -  stimer;
	printf ("%.f msec \n", seconds);
	return 0;

#else
	vector<int> pgrank;
	//Without reservering this is not working maybe becasue memory is allocated only in one process.
	pgrank.resize(NODES);
	for(int i=0; i<NODES; i++) {
		pgrank[i+1] = 10; 
	}
	vector <vector<int> >  partations;
	partations.resize(size);

	/*
	   7
	   |
	   1--2--3
	   |  |  |
	   4  5  6
	 */


	if(rank == 0) {
		partations[rank].resize(2);
		partations[rank][0] = 1;
		partations[rank][1] = 4;
	}

	if(rank == 1) {
		partations[rank].resize(3);
		partations[rank][0] = 2;
		partations[rank][1] = 5;
		partations[rank][2] = 7;
	}

	if(rank == 2) {
		partations[rank].resize(2);
		partations[rank][0] = 3;
		partations[rank][1] = 6;
	}


	int ncnt = partations[rank].size();
	for(int i=0; i<ncnt; i++) {
		cout<<"Element :" << partations[rank][i] << endl;
	}


	if(rank == 0) {

		from = 1;
		tag = 303;
		cout<<"Ready for Receiving :" << rank << endl;
		MPI_Recv(&num, 1, MPI_INT, from, tag, MPI_COMM_WORLD, &status);
		printf("Process %d received %d from %d\n", rank, num, from );


		num = 2;
		next = 1;
		tag = 101;
		printf("Process %d sending req for %d to %d\n", rank, num, next);
		MPI_Send(&num, 1, MPI_INT, next, tag, MPI_COMM_WORLD); 
	} else if(rank == 1) {
		num = 1;
		tag = 303;
		next = 0;
		printf("Process %d sending req for %d to %d\n", rank, num, next);
		MPI_Send(&num, 1, MPI_INT, next, tag, MPI_COMM_WORLD); 

		num = 3;
		tag = 404;
		next = 2;
		printf("Process %d sending req for %d to %d\n", rank, num, next);
		MPI_Send(&num, 1, MPI_INT, next, tag, MPI_COMM_WORLD); 

		tag = 101;
		from = 0;
		cout<<"Ready for Receiving :" << rank << endl;
		MPI_Recv(&num, 1, MPI_INT, from, tag, MPI_COMM_WORLD, &status);
		printf("Process %d received %d from %d\n", rank, num, from);


		tag = 202;
		from = 2;
		cout<<"Ready for Receiving :" << rank << endl;
		MPI_Recv(&num, 1, MPI_INT, from, tag, MPI_COMM_WORLD, &status);
		printf("Process %d received %d from %d\n", rank, num, from);

	} else {
		tag = 404;
		from = 1;
		cout<<"Ready for Receiving :" << rank << endl;
		MPI_Recv(&num, 1, MPI_INT, from, tag, MPI_COMM_WORLD, &status);
		printf("Process %d received %d from %d\n", rank, num, from);

		num = 2;
		tag = 202;
		next = 1;
		printf("Process %d sending req for %d to %d\n", rank, num, next);
		MPI_Send(&num, 1, MPI_INT, next, tag, MPI_COMM_WORLD); 
	}

#endif
	/* Quit. Finalize is must. */
	MPI_Finalize();
	return 0;
}
