#include "GraphReader.h"
#include "Graph.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <cilkview.h>
#include <vector>
#include "trace.h"
#include <cmath>
using namespace std;

//#define PAR_CC
//#define PAR_CC2
#define PAR_CC3

int cilk_main(int argc, char *argv[]){
//int main(int argc, char *argv[]){
	if(argc < 3){
		cout << "usage: bfs_serial <graph input filename> <graph output filename>" << endl;
		return 1;
	}

	try {
                //trace_init(true);
                trace_init(false);

		GraphReader *gr = new GraphReader();
		Graph *G = gr->read(argv[1]);
		
		//int cores = cilk::current_worker_count();
		//int maxstealattempts = 12, minstealsize = 30;
		int nvertics = G->numvertices();
		//cout << "nver " <<nvertics <<endl;
		// Addded one as the vertics start from 1
		
		srand(time(NULL));
		
#ifdef PAR_CC		
		int *M = new int[nvertics+1];
#endif
		int *L = new int[nvertics+1];
		int *V = new int[nvertics+1];

		edge* E =  G->getedge();
		
		for(int i = 0; i < nvertics; i++) {
			V[i] = i+1;
		}


		for(int i=1; i<=nvertics; i++){
			L[i] = i;
		}


#ifdef PAR_CC3
                bool 	*phd = new bool[nvertics+1];
                int 	*N = new int[nvertics+1];
                bool 	*U = new bool[nvertics+1];

                for(int i = 0; i < nvertics; i++){
                        int v = V[i];
                        phd[v] = true;
                        N[v] = v;
                        U[v] = false;
                }

		float log_1_alpha_n = log(nvertics)/log(1/alpha1);
                int d_max = ceil((1.0/4.0)*log_1_alpha_n);
 
#endif
		
#ifdef PAR_CC
		//cilk::cilkview cv;
		//cv.reset();
		//cv.start();
		int start_time = cilk::get_milliseconds();
		parRandCC(nvertics, G->numedges(),  E, L, M);
		cout << "time of execution is " << 
		(((float)(cilk::get_milliseconds() - start_time))/1000) << "secs" << endl;
	
		//cv.stop();
#elif PAR_CC2
		//cilk::cilkview cv;
		//cv.reset();
		//cv.start();
		int start_time = cilk::get_milliseconds();
		parRandCC2(nvertics, G->numedges(), nvertics+1, V, E, L);
		cout << "time of execution is " << 
		(((float)(cilk::get_milliseconds() - start_time))/1000) << "secs" << endl;
		//cv.stop();
#else

                //debug <<"alpha=" <<alpha1 << ", log_1_alpha_n="<<log_1_alpha_n <<", d_max=" <<d_max <<  endl;
                //debug << "before parRandCC3" <<endl;
		
		//cilk::cilkview cv;
		//cv.reset();
		//cv.start();
		int start_time = cilk::get_milliseconds();
                parRandCC3(nvertics, G->numedges(), nvertics+1, V, E, L, phd, N, U, 0, d_max);
		cout << "time of execution is " << 
		(((float)(cilk::get_milliseconds() - start_time))/1000) << "secs" << endl;
		//cv.stop();
#endif
		/*std::fstream outfile(argv[2]);
		cout << nvertics << endl;*/
#ifdef PAR_CC
		gr->dumpoutput(M, nvertics);
#else 
		gr->dumpoutput(L, nvertics);
#endif	
		//for(int i = 1; i<=nvertics; i++){
		//	cout << M->at(i) << endl;
		//}


#ifdef PAR_CC		
		delete[] M;
#endif

#ifdef PAR_CC3
                delete [] phd;
                delete [] N;
                delete [] U;
#endif

		delete[] L;
		delete[] V;
		//outfile.close();
#if 0
		string outfilename(argv[2]);
		gr->dump(outfilename, G);	
#endif

#if 0
		cilk::cilkview cv;
		cv.reset();
		cv.start();
		int start_time = cilk::get_milliseconds();
		//Your program will be called from here.
		cv.stop();
		//cout << "time of execution is " << 
		(((float)(cilk::get_milliseconds() - start_time))/1000) << "secs" << endl;
#endif
		delete G;
		delete gr;
		trace_done();
	}	
	catch (exception& e){
		cout << "invalid file name or graph input format" << e.what() <<endl;
	}
	
	return 0;
}
