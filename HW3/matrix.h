#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <string>
#include <iostream>
#include <chrono>
#include <iomanip>



using namespace std;


typedef struct _matrix {
    int i;
    int j;
    int n;
    _matrix(int _i, int _j, int _n) : i(_i), j(_j), n(_n){}
} matrixinfo;




class matrix {
public:
	int *get_matrix1();
	int *get_matrix2();
	void read_matrix(string fname, int type);
	void dump_matrix(int *matrix);
	void mz_init();
	void mz_alloc();

	void recMM(matrixinfo x, matrixinfo y, matrixinfo z);

	void Iter_MM_ijk(matrixinfo x, matrixinfo y, matrixinfo z);
	void Iter_MM_ikj(matrixinfo x, matrixinfo y, matrixinfo z);
	void Iter_MM_jik(matrixinfo x, matrixinfo y, matrixinfo z);
	void Iter_MM_jki(matrixinfo x, matrixinfo y, matrixinfo z);
	void Iter_MM_kij(matrixinfo x, matrixinfo y, matrixinfo z);
	void Iter_MM_kji(matrixinfo x, matrixinfo y, matrixinfo z);
#if 0
	void Iter_MM_ikj();
	void Iter_MM_jik();
	void Iter_MM_jki();
	void Iter_MM_kij();
	void Iter_MM_kji();
#endif


	int *mx, *my, *mz;
	int matrix_sz;
};


